package com.wyq.elasticsearch.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.wyq.elasticsearch.bean.Person;
import com.wyq.elasticsearch.bean.ResultVo;
import com.wyq.elasticsearch.bean.SearchHotelInfoDTO;
import com.wyq.elasticsearch.bean.SearchPersonResult;
import com.wyq.elasticsearch.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * @Author: wyq
 * @Desc:
 * @Date: 2019/11/29 12:36
 **/
@RestController
@RequestMapping("/es")
public class PersonController {

    private static final Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private PersonService personService;


    /**
     * 检验索引是否存在
     *
     * @param indexName
     * @return
     */
    @RequestMapping("check/{indexName}")
    public ResultVo checkIndexExists(@PathVariable("indexName") String indexName) {
        if (StrUtil.isEmpty(indexName)) {
            return ResultVo.missParameters();
        }
        final boolean exists = personService.checkIndexExist(indexName);
        return ResultVo.success(exists);
    }


    /**
     * 单条添加
     *
     * @param person
     * @param indexName
     * @return
     */
    @RequestMapping("/insert")
    public ResultVo singleAdd(@RequestBody Person person, @RequestParam("index") String indexName) {
        if (ObjectUtil.isNull(person) || ObjectUtil.isEmpty(person)) {
            return ResultVo.missParameters();
        }
        try {
            final boolean result = personService.singleAdd(person, indexName);
            return ResultVo.success(result);
        } catch (Exception ex) {
            logger.error("ES查询失败,失败信息:{}", ex.getMessage());
        }
        return ResultVo.failed();
    }


    /**
     * 插入多条
     *
     * @param people
     * @param indexName
     * @return
     */
    @RequestMapping("/inserts")
    public ResultVo batchInsert(@RequestBody List<Person> people, @RequestParam("index") String indexName) {
        if (StrUtil.isEmpty(indexName)) {
            return ResultVo.missParameters();
        }
        try {
            final boolean success = personService.batchAdd(people, indexName);
            return ResultVo.success(success);
        } catch (Exception ex) {
            logger.error("ES查询失败,失败信息:{}", ex.getMessage());
        }
        return ResultVo.failed();
    }


    /**
     * 批量更新
     *
     * @param people
     * @param indexName
     * @return
     */
    @RequestMapping("/updates")
    public ResultVo batchUpdate(@RequestBody List<Person> people, @RequestParam("index") String indexName) {
        if (StrUtil.isEmpty(indexName) || CollectionUtil.isEmpty(people)) {
            return ResultVo.missParameters();
        }
        try {
            final boolean success = personService.batchUpdate(people, indexName);
            return ResultVo.success(success);
        } catch (Exception ex) {
            logger.error("ES更新失败,失败信息:{}", ex.getMessage());
        }
        return ResultVo.failed();
    }


    /**
     * 删除索引
     *
     * @param indexName
     * @return
     */
    @RequestMapping("del/{indexName}")
    public ResultVo deleteIndex(@PathVariable("indexName") String indexName) {
        if (StrUtil.isEmpty(indexName)) {
            return ResultVo.missParameters();
        }
        final boolean exists = personService.deleteIndex(indexName);
        return ResultVo.success(exists);
    }


    /**
     * 获取所有数据
     *
     * @param indexName
     * @return
     */
    @RequestMapping("/getAll")
    public ResultVo getAllPeople(@RequestParam("indexName") String indexName) {
        if (StrUtil.isEmpty(indexName)) {
            return ResultVo.missParameters();
        }
        try {
            final SearchPersonResult personResult = personService.getAll(indexName);
            return ResultVo.success(personResult.getPeople(), personResult.getCount());
        } catch (Exception ex) {
            logger.error("ES查询失败,失败信息:{}", ex.getMessage());
        }
        return ResultVo.failed();
    }


    /**
     * 根据名字获取人
     *
     * @param personName
     * @return
     */
    @RequestMapping("/getByName")
    public ResultVo getPersonByName(@RequestParam("name") String personName, @RequestParam("indexName") String indexName) {
        if (StrUtil.isEmpty(indexName)) {
            return ResultVo.missParameters();
        }
        try {
            final Person person = personService.searchByName(indexName, personName);
            return ResultVo.success(person);
        } catch (Exception ex) {
            logger.error("ES查询失败,失败信息:{}", ex.getMessage());
        }
        return ResultVo.failed();
    }


    /**
     * 根据经纬度和距离查询
     *
     * @param indexName
     * @param lat
     * @param lon
     * @param distance
     * @return
     */
    @RequestMapping("/geo/{lat}/{lon}/{distance}")
    public ResultVo searchByGeo(@RequestParam("indexName") String indexName, @PathVariable("lat") Double lat, @PathVariable("lon") Double lon,
                                @PathVariable("distance") Long distance) {
        if (StrUtil.isEmpty(indexName) || ObjectUtil.isNull(lat) || ObjectUtil.isNull(lon) || ObjectUtil.isNull(distance)) {
            ResultVo.missParameters();
        }
        try {
            final Person person = personService.geoSearch(lat, lon, indexName, distance);
            return ResultVo.success(person, 1);
        } catch (Exception ex) {
            logger.error("ES查询失败,失败信息:{}", ex.getMessage());
        }
        return ResultVo.failed();
    }


    /**
     * 多条件搜索
     *
     * @param searchHotelInfoDTO 搜索条件
     * @return
     */
    @RequestMapping("/multiSearch")
    public ResultVo multiSearch(@RequestBody SearchHotelInfoDTO searchHotelInfoDTO) {

        SearchPersonResult searchHotelInfoResponse = null;
        try {
            searchHotelInfoResponse = personService.multiSearch(searchHotelInfoDTO.getPageIndex(), searchHotelInfoDTO.getPageSize(), searchHotelInfoDTO.getCondition(),
                    searchHotelInfoDTO.getOrderBy(), searchHotelInfoDTO.getSort());
            return ResultVo.success(searchHotelInfoResponse.getPeople(), Integer.parseInt(String.valueOf(searchHotelInfoResponse.getCount())));
        } catch (Exception ex) {
            logger.error("ES查询失败,失败信息:{}", ex.getMessage());
        }
        return ResultVo.failed();

    }


}
