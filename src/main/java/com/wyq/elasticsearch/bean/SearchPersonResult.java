package com.wyq.elasticsearch.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


/**
 * @Author: wyq
 * @Desc:
 * @Date: 2019/11/29 16:44
 **/
@Data
@AllArgsConstructor
public class SearchPersonResult {

    private List<Person> people;

    private long count;
}
