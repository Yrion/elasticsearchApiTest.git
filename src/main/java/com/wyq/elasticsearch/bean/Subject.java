package com.wyq.elasticsearch.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: wyq
 * @Desc:
 * @Date: 2019/11/28 18:28
 **/
@Data
public class Subject implements Serializable {

    private String subjectName;

    private int subjectType;

    private Double subjectScore;
}
