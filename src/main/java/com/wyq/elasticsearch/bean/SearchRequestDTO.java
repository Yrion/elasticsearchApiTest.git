package com.wyq.elasticsearch.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: wyq
 * @Desc: 搜索请求DTO
 * @Date: 2019/11/14 11:19
 **/
@Data
public class SearchRequestDTO implements Serializable {


    /**
     * 搜索条件
     */
    private String condition;

    /**
     * 排序依据 1:分数 2:年龄
     */
    private Integer orderBy;

    /**
     * 排序 1：降序 2：升序
     */
    private Integer sort;

    /**
     * 当前页
     */
    private Integer pageIndex;

    /**
     * 页数大小
     */
    private Integer pageSize;

}
