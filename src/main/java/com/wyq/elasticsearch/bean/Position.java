package com.wyq.elasticsearch.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: wyq
 * @Desc:
 * @Date: 2019/11/30 10:45
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Position implements Serializable {

    private double lat;
    private double lon;
}
