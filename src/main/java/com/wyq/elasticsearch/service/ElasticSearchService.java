package com.wyq.elasticsearch.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.wyq.elasticsearch.bean.Person;
import com.wyq.elasticsearch.contants.CommonConstants;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Author: wyq
 * @Desc:
 * @Date: 2019/11/29 14:27
 **/
@Service
public class ElasticSearchService {


    private static final Logger logger = LoggerFactory.getLogger(ElasticSearchService.class);

    /**
     * ES客户端
     */
    @Autowired
    protected RestHighLevelClient client;


    /**
     * 检查index是否存在
     *
     * @param indexName
     * @return
     */
    public boolean checkIndex(String indexName) {
        GetIndexRequest request = new GetIndexRequest(indexName);
        boolean exists = false;
        try {
            exists = client.indices().exists(request, RequestOptions.DEFAULT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return exists;
    }

    /**
     * 获取source
     *
     * @param document
     * @return
     */
    protected IndexRequest getInsertSource(Person document, String indexName) {
        final String personJson = JSONUtil.toJsonStr(document);
        return new IndexRequest(indexName).source(personJson, XContentType.JSON);
    }

    /**
     * 批量请求
     *
     * @param request
     * @return
     */
    protected Boolean bulk(BulkRequest request) {
        BulkResponse bulkResponse = null;
        try {
            bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
        } catch (Exception ex) {
            logger.error("bulk异常,异常信息:{}", ex.getMessage());
            ex.printStackTrace();
        }
        return bulkResponse == null ? false : !bulkResponse.hasFailures();
    }


    /**
     * 按照索引名更新
     *
     * @param indexName
     * @return
     */
    protected boolean delete(String indexName) {

        final DeleteRequest deleteRequest = new DeleteRequest(indexName);
        DeleteResponse response = null;
        try {
            response = client.delete(deleteRequest, RequestOptions.DEFAULT);
        } catch (Exception ex) {
            logger.error("delete异常信息:{}", ex.getMessage());
            ex.printStackTrace();
        }
        return response == null ? false : response.status().equals(DocWriteResponse.Result.DELETED);

    }

    /**
     * 布尔查询
     *
     * @param orderBy
     * @param sort
     * @return
     */
    protected Map<Integer, Object> boolSearch(Integer pageIndex, Integer pageSize, Map<String, String> conditionMap, Integer orderBy, Integer sort) throws Exception {
        {
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
            GeoPoint point = null;
            boolean isSort = false;
            if (MapUtil.isEmpty(conditionMap)) {
                return new HashMap<>();
            }
            for (Map.Entry<String, String> entry : conditionMap.entrySet()) {
                QueryBuilder queryBuilder = null;
                final boolean position = CommonConstants.POSITION.equals(entry.getKey());
                final boolean personName = CommonConstants.PERSON_NAME.equals(entry.getKey());
                final boolean age = CommonConstants.FIELD_AGE.equals(entry.getKey());
                final boolean sportAndSubject = CommonConstants.SPORT_AND_SUBJECT.equals(entry.getKey());
                if (personName) {
                    queryBuilder = QueryBuilders.fuzzyQuery(entry.getKey(), entry.getValue());
                } else if (position) {
                    isSort = true;
                    final JSONObject positionValue = JSONUtil.parseObj(entry.getValue());
                    final Map<Integer, Object> pointAndDistance = resolvePosition(positionValue);
                    point = (GeoPoint) pointAndDistance.get(CommonConstants.GEO_POINT);
                    final String distance = (String) pointAndDistance.get(CommonConstants.DISTANCE);
                    queryBuilder = QueryBuilders.geoDistanceQuery(CommonConstants.POSITION)
                            .distance(distance, DistanceUnit.KILOMETERS)
                            .point(point).geoDistance(GeoDistance.ARC);
                } else if (age) {
                    final String termsValue = entry.getValue();
                    String minAge = "";
                    String maxAge = "";
                    if (StrUtil.isNotEmpty(termsValue)) {
                        final JSONArray jsonArray = JSONUtil.parseArray(termsValue);
                        final List<String> ageList = jsonArray.toList(String.class);
                        if (CollectionUtil.isNotEmpty(ageList)) {
                            minAge = ageList.get(0);
                            maxAge = ageList.get(1);
                        }
                    }
                    queryBuilder = QueryBuilders.rangeQuery(CommonConstants.FIELD_AGE).from(minAge).to(maxAge).includeLower(true).includeUpper(true);
                } else if (sportAndSubject) {
                    queryBuilder = getSportAndSubject(entry);
                } else {
                    final String termsValue = entry.getValue();
                    if (StrUtil.isNotEmpty(termsValue)) {
                        queryBuilder = QueryBuilders.termQuery(entry.getKey(), termsValue);
                    }
                }
                if (queryBuilder != null) {
                    boolQueryBuilder = boolQueryBuilder.must(queryBuilder);
                }
            }
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(boolQueryBuilder);
            SortBuilder order = getSortBuilder(orderBy, point, sort, isSort);
            searchSourceBuilder.sort(order);
            if (ObjectUtil.isNotNull(pageIndex)) {
                searchSourceBuilder.from(pageIndex);
            }
            if (ObjectUtil.isNotNull(pageSize)) {
                searchSourceBuilder.size(pageSize);
            } else {
                searchSourceBuilder.size(CommonConstants.DEFAULT_SIZE);
            }
            final Map<Integer, Object> resultMap = searchAndGetResult(searchSourceBuilder, CommonConstants.DEFAULT_INDEX_NAME);
            return resultMap;
        }
    }


    /**
     * 获取价格和日期builder
     *
     * @param entry
     * @return
     */
    private  QueryBuilder getSportAndSubject(Map.Entry<String, String> entry) {
        final JSONObject sportAndSubject = new JSONObject(entry.getValue());
        final JSONObject sport = (JSONObject) sportAndSubject.get("sport");
        final JSONObject subject = (JSONObject) sportAndSubject.get("subject");
        //运动类型
        String sportType = (String) sport.get("type");
        //科目类型
        final String subjectType = (String) subject.get("type");
        //最低分数
        final String minScore = (String) subject.get("minScore");
        BoolQueryBuilder allQueryBuilder = new BoolQueryBuilder();
        if (StrUtil.isNotEmpty(sportType)) {
            TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery(CommonConstants.HOBBY_SPORT_TYPE, sportType);
            allQueryBuilder.should(termQueryBuilder);
        }
        //科目类型
        if (StrUtil.isNotEmpty(subjectType) && StrUtil.isNotEmpty(minScore)) {
            final BoolQueryBuilder boolQueryBuilder = getSubjectAndScoreBuilder(sportType, minScore, null);
            allQueryBuilder.should(boolQueryBuilder);
        }
        return QueryBuilders.nestedQuery(CommonConstants.HOBBY_SPORT_TYPE, allQueryBuilder, ScoreMode.Max);
    }


    protected   BoolQueryBuilder getSubjectAndScoreBuilder(String sportType, String minScore, String maxScore) {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery(CommonConstants.HOBBY_SUBJECT_TYPE, sportType);
        boolQueryBuilder.must(termQueryBuilder);
        final int score = StrUtil.isEmpty(maxScore) ? CommonConstants.MAX_SCORE : Integer.parseInt(maxScore);
        final RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(CommonConstants.HOBBY_SUBJECT_SCORE_TYPE).from(minScore).to(score).includeLower(true).includeUpper(true);
        boolQueryBuilder.must(rangeQueryBuilder);
        return boolQueryBuilder;
    }

    /**
     * 解析距离字段
     *
     * @param positionValue
     * @return
     */
    private Map<Integer, Object> resolvePosition(JSONObject positionValue) {
        final HashMap<Integer, Object> resultMap = new HashMap<>();
        final Double lat = (Double) positionValue.get("lat");
        final Double lon = (Double) positionValue.get("lon");
        final String distance = (String) positionValue.get("distance");
        final GeoPoint point = new GeoPoint(lat, lon);
        resultMap.put(CommonConstants.GEO_POINT, point);
        resultMap.put(CommonConstants.DISTANCE, distance);
        return resultMap;
    }


    /**
     * 获取排序维度
     *
     * @param
     * @param orderBy
     * @param point
     * @param isSort
     * @return
     */
    protected static SortBuilder getSortBuilder(Integer orderBy, GeoPoint point, Integer sort, boolean isSort) {
        final SortOrder sortOrder = CommonConstants.DESC.equals(sort) ? SortOrder.DESC : SortOrder.ASC;
        //默认按照分数排序
        SortBuilder order = new ScoreSortBuilder().order(sortOrder);
        if (CommonConstants.ORDER_BY_SCORE.equals(orderBy)) {
            order = new FieldSortBuilder(CommonConstants.FIELD_SCORE).order(sortOrder);
        } else if (CommonConstants.ORDER_BY_DISTANCE.equals(orderBy) && ObjectUtil.isNotNull(point) && isSort) {
            order = new GeoDistanceSortBuilder(CommonConstants.POSITION, point).order(sortOrder);
        } else if (CommonConstants.ORDER_BY_AGE.equals(orderBy)) {
            order = new FieldSortBuilder(CommonConstants.FIELD_AGE).order(sortOrder);
        }
        return order;
    }


    /**
     * 获取source
     *
     * @param document
     * @return
     */
    protected UpdateRequest getUpdateSource(Person document, String indexName) {
        final String personJson = JSONUtil.toJsonStr(document);
        final List<String> ids = getPersonIdById(document.getId(), indexName);
        if (CollectionUtil.isNotEmpty(ids)) {
            UpdateRequest request = new UpdateRequest(indexName, ids.get(0)).doc(personJson, XContentType.JSON);
            return request;
        }
        return null;
    }


    /**
     * 搜索并且获取结果
     *
     * @param searchSourceBuilder
     * @param indexName
     * @return
     */
    protected Map<Integer, Object> searchAndGetResult(SearchSourceBuilder searchSourceBuilder, String indexName) throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(indexName);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = null;
        logger.info("查询的DSL语句为:{}", searchSourceBuilder.toString());
        try {
            response = client.search(searchRequest, RequestOptions.DEFAULT);
        } catch (Exception ex) {
            logger.error("查询时出错,出错信息为:{}", ex.getMessage());
            throw ex;
        }
        if (ObjectUtil.isNotNull(response)) {
            logger.info("ES返回的数据为:{}", response.toString());
        }
        final Map<Integer, Object> resultMap = new HashMap<>();
        final JSONArray jsonArray = new JSONArray();
        resultMap.put(1, getJsonArray(response, jsonArray).get(CommonConstants.COUNT_KEY));
        resultMap.put(2, jsonArray.toString());
        return resultMap;
    }


    /**
     * 获取json获取
     *
     * @param response
     * @param jsonArray
     * @return
     */
    private Map<Integer, Object> getJsonArray(SearchResponse response, JSONArray jsonArray) {
        final Map<Integer, Object> resultMap = new HashMap<>();
        if (ObjectUtil.isNull(response)) {
            return resultMap;
        }
        final SearchHits hits = response.getHits();
        Long count = 0L;
        List<String> ids = new ArrayList<>();
        if (ObjectUtil.isNotNull(hits)) {
            count = hits.getTotalHits().value;
            final SearchHit[] hitsArray = hits.getHits();
            if (ArrayUtil.isNotEmpty(hitsArray)) {
                for (SearchHit hit : hitsArray) {
                    final Map<String, Object> sourceAsMap = hit.getSourceAsMap();
                    final String id = hit.getId();
                    final JSONObject jsonObject = new JSONObject();
                    for (Map.Entry<String, Object> entry : sourceAsMap.entrySet()) {
                        jsonObject.put(entry.getKey(), entry.getValue());
                    }
                    jsonArray.add(jsonObject);
                    ids.add(id);
                }
            }
        }
        resultMap.put(CommonConstants.COUNT_KEY, count);
        resultMap.put(CommonConstants.ID_KEY, ids);
        return resultMap;
    }


    /**
     * 根据builder去查询
     *
     * @param from
     * @param size
     * @param matchQueryBuilder
     */
    private Map<Integer, Object> queryByBuilder(Integer from, Integer size, QueryBuilder matchQueryBuilder, String indexName) {
        final Map<Integer, Object> resultMap = new HashMap<>();
        final SearchSourceBuilder searchSourceBuilder = createSearchSourceBuilder(from, size, matchQueryBuilder);
        SearchRequest searchRequest = getSearchRequest(searchSourceBuilder, indexName);
        SearchResponse searchResponse = null;
        try {
            searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        } catch (Exception ex) {
            logger.error("搜索时异常,异常信息:{}", ex.getMessage());
            ex.printStackTrace();
        }
        getResultMap(resultMap, searchResponse);
        return resultMap;
    }


    private List<String> getPersonIdById(String id, String indexName) {
        final Map<Integer, Object> resultMap = search(null, null, "id", id, indexName);
        if (MapUtil.isNotEmpty(resultMap)) {
            final List<String> ids = (List<String>) resultMap.get(CommonConstants.ID_KEY);
            return ids;
        }
        return Collections.EMPTY_LIST;
    }


    /**
     * 创建searchSourceBuilder
     *
     * @param from
     * @param size
     * @param matchQueryBuilder
     */
    private SearchSourceBuilder createSearchSourceBuilder(Integer from, Integer size, QueryBuilder matchQueryBuilder) {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(matchQueryBuilder);
        if (ObjectUtil.isNotNull(from)) {
            sourceBuilder.from(from);
        }
        if (ObjectUtil.isNotNull(size)) {
            sourceBuilder.size(size);
        }
        //默认超时1分钟
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        return sourceBuilder;
    }

    /**
     * 复杂查询
     *
     * @param from
     * @param size
     */
    public Map<Integer, Object> search(Integer from, Integer size, String queryKey, String queryValue, String indexName) {
        final QueryBuilder matchQueryBuilder = QueryBuilders.matchQuery(queryKey, queryValue);
        return queryByBuilder(from, size, matchQueryBuilder, indexName);
    }


    private void getResultMap(Map<Integer, Object> resultMap, SearchResponse searchResponse) {
        final JSONArray jsonArray = new JSONArray();
        final Map<Integer, Object> searchResult = getJsonArray(searchResponse, jsonArray);
        Long count = (Long) searchResult.get(CommonConstants.COUNT_KEY);
        resultMap.put(CommonConstants.COUNT_KEY, count);
        final String json = jsonArray.toString();
        resultMap.put(CommonConstants.DATA_KEY, json);
        resultMap.put(CommonConstants.ID_KEY, searchResult.get(CommonConstants.ID_KEY));
    }


    protected SearchRequest getSearchRequest(SearchSourceBuilder searchSourceBuilder, String indexName) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(indexName);
        return searchRequest;
    }


    /**
     * 经纬度查询
     *
     * @param indexName
     * @param from
     * @param size
     * @param lat       经度
     * @param lon       维度
     * @return
     */
    public Map<Integer, Object> locationSearch(String indexName, Integer from, Integer size, double lat, double lon, String distance) {
        final QueryBuilder distanceQueryBuilder = QueryBuilders.geoDistanceQuery("position")
                .distance(distance, DistanceUnit.KILOMETERS)
                .point(lat, lon).geoDistance(GeoDistance.ARC);
        return queryByBuilder(from, size, distanceQueryBuilder, indexName);
    }


    /**
     * 匹配所有的查询
     *
     * @param indexName
     * @return
     */
    public Map<Integer, Object> matchAllQuery(String indexName, Integer from, Integer size) {
        final QueryBuilder matchQueryBuilder = QueryBuilders.matchAllQuery();
        return queryByBuilder(from, size, matchQueryBuilder, indexName);
    }


    /**
     * 插入指定的index数据
     *
     * @return
     */
    public Boolean insert(String indexName, Object personDocument) {
        IndexResponse response = null;
        try {
            indexName = StrUtil.isEmpty(indexName) ? CommonConstants.DEFAULT_INDEX_NAME : indexName;
            final IndexRequest request = new IndexRequest(indexName);
            final String personJson = JSONUtil.toJsonStr(personDocument);
            request.source(personJson, XContentType.JSON);
            response = client.index(request, RequestOptions.DEFAULT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        final DocWriteResponse.Result result = response.getResult();
        return result.equals(DocWriteResponse.Result.CREATED);
    }


    /**
     * 通过index更新
     *
     * @param index
     * @return
     */
    public boolean updateByIndex(String index, Person document) {
        UpdateResponse updateResponse = null;
        try {
            final List<String> ids = getPersonIdById(document.getId(), index);
            if (CollectionUtil.isNotEmpty(ids)) {
                for (String id : ids) {
                    final String json = JSONUtil.toJsonStr(document);
                    UpdateRequest request = new UpdateRequest(index, id).doc(json, XContentType.JSON);
                    updateResponse = client.update(request, RequestOptions.DEFAULT);
                    if (!DocWriteResponse.Result.UPDATED.equals(updateResponse.getResult())) {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
