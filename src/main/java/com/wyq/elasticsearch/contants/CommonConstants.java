package com.wyq.elasticsearch.contants;


/**
 * @Author: wyq
 * @Desc: 公共常量
 * @Date: 2019/11/8 17:06
 **/
public class CommonConstants {

    /**
     * map的计数key
     */
    public static final int COUNT_KEY =1;

    /**
     * map的dataKey
     */
    public static final int DATA_KEY =2;

    /**
     * 获取数据的Id的key
     */
    public static final int ID_KEY=100;

    /**
     * 位置信息
     */
    public static final String POSITION = "position";

    public static final Integer GEO_POINT = 1;

    public static final Integer DISTANCE = 2;


    /**
     * 按照星级排序
     */
    public static final Integer ORDER_BY_AGE =2;

    /**
     * 按照分数排序
     */
    public static final Integer ORDER_BY_SCORE =3;

    /**
     * 按照距离排序
     */
    public static final Integer ORDER_BY_DISTANCE = 4;

    /**
     * 降序
     */
    public static final Integer DESC = 1;

    /**
     * 按照得分
     */
    public static final String FIELD_SCORE = "score";

    /**
     * 按照星级
     */
    public static final String FIELD_AGE = "age";

    /**
     * 特色
     */
    public static final String FIELD_FEATURES = "feature";

    /**
     * 设施
     */
    public static final String FIELD_FACILITY = "facility";

    /**
     * 酒店名称
     */
    public static final String PERSON_NAME = "name";

    /**
     * 最高分数
     */
    public static final Integer MAX_STAR = 5;


    /**
     * 日期和价格
     */
    public static final String SPORT_AND_SUBJECT = "sportAndSubject";


    /**
     * 运动类型
     */
    public static final String HOBBY_SPORT_TYPE = "hoobby.sports.sportType";

    /**
     * 学科类型
     */
    public static final String HOBBY_SUBJECT_TYPE = "hoobby.sports.subjectType";

    /**
     * 分数类型
     */
    public static final String HOBBY_SUBJECT_SCORE_TYPE = "hoobby.sports.subjectScore";


    /**
     * 预订日期
     */
    public static final String  NESTED_BOOK_DATE ="roomInfoList.priceInfos.saleDate";

    /**
     * 价格
     */
    public static final String  NESTED_BOOK_PRICE ="roomInfoList.priceInfos.price";


    /**
     * 房态
     */
    public static final String  NESTED_BOOK_BOOK_STATE = "roomInfoList.priceInfos.roomState";


    /**
     * 可订的房态
     */
    public static final String  CAN_BOOK ="1";

    /**
     * 城市索引
     */
    public static final String CITY_INDEX = "city";

    /**
     * 商圈索引
     */
    public static final String ZONE_INDEX = "zone";

    /**
     * 区域索引
     */
    public static final String AREA_INDEX = "area";


    public static final String NULL = "null" ;

    public static final String EMPTY = "''" ;

    public static final int DEFAULT_SIZE = 50;

    /**
     * 最高分数
     */
    public static final Integer MAX_SCORE =100 ;

    /**
     * 酒店索引
     */
    public static String DEFAULT_INDEX_NAME = "hotelindex" ;


    /**
     * 城市code
     */
    public static final String CITY_CODE = "city_code";
}
